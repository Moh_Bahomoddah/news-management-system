import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as firebase from 'firebase/app'
import 'firebase/firestore'
import AlertCmp from './components/share/alert.vue'
import btnEditing from './components/News/btnEditing.vue'


Vue.config.productionTip = false

Vue.component('app-alert', AlertCmp)
Vue.component('btn-editing', btnEditing)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: "AIzaSyDzOCPMJQwbg9SENMZiy32y09WhRsUVS20",
      authDomain: "news-management-system.firebaseapp.com",
      databaseURL: "https://news-management-system.firebaseio.com",
      projectId: "news-management-system",
      storageBucket: "news-management-system.appspot.com",
      messagingSenderId: "52962739566",
      appId: "1:52962739566:web:c8f51ad465aa1add3e6d7f",
      measurementId: "G-89TLJMWF8C"
    })
    
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadNewsItems')
  }
}).$mount('#app')
