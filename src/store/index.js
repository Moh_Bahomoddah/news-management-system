import Vue from 'vue'
import Vuex from 'vuex'

import newsItem from './NewsItem'
import user from './User'
import share from './share'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    newsItem,
    user: user,
    share: share
  }
})
