import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import NewsPage from '@/views/NewsPage'
import NewsItem from '@/components/News/NewsItem'
import CreateNewsItem from '@/components/News/CreateNewsItem'
import Signup from '@/components/Users/Signup'
import Signin from '@/components/Users/Signin'
import AuthGuard from './auth-guard'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/newspage',
      name: 'newspage',
      component: NewsPage
    },
    {
      path: '/newspage/new',
      name: 'CreateNewsItem',
      component: CreateNewsItem,
      props: true,
      beforeEnter: AuthGuard
    },
    {
      path: '/newspage/:id',
      name: 'NewsItem',
      props: true,
      component: NewsItem
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    }
  ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
